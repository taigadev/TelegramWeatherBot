﻿using Telegram.Bot;
using Telegram.Bot.Types;

namespace TelegramWeatherBot.Types
{
    public abstract class Command
    {
        public abstract string Name { get; }
        public abstract string Response { get; }
        public abstract void Execute(Message message, TelegramBotClient Client);
    }
}
