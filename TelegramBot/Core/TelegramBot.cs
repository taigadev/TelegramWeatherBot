﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telegram.Bot;
using TelegramWeatherBot.Types;
using TelegramWeatherBot.Core.Commands;
using Telegram.Bot.Types.Enums;

namespace TelegramWeatherBot.Core
{
    public static class TelegramBot
    {
        public static TelegramBotClient client;
        private static List<Command> CommandsList { get; set; }
        public static IReadOnlyList<Command> Commands { get => CommandsList.AsReadOnly(); }
        public static void Main()
        {
            client = new TelegramBotClient(Settings.TelegramBotToken);
            LoadBotCommands();
            Weather.LoadData();

            client.OnMessage += OnMessageRecieved;

            client.StartReceiving();
            Console.WriteLine("TelegramWeatherBot started. Type stop to stop bot...");

            string lane = "";
            while (lane != "stop")
            {
                lane = Console.ReadLine();
            }

            Weather.SaveData();
            client.StopReceiving();
        }

        private static void OnMessageRecieved(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            if (e == null) return;
            var message = e.Message;

            if (message == null || message.Type != MessageType.TextMessage) return;

            var query = Commands.Where(x => message.Text.ToLower().StartsWith($"/{x.Name}"));
            if (query.Count() == 1)
                foreach (var command in query)
                    command.Execute(message, client);
        }
        private static void LoadBotCommands()
        {
            CommandsList = new List<Command>();
            CommandsList.Add(new CmHello());
            CommandsList.Add(new CmHelp());
            CommandsList.Add(new CmWeather());
        }
    }
}