﻿using System;
using System.Net;
using System.IO;
using GoogleMaps.LocationServices;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace TelegramWeatherBot.Core
{
    /// <summary>
    /// Класс для получения погоды по названию места
    /// </summary>
    [Serializable]
    public static class Weather
    {
        private static string Path { get; set; } = Environment.CurrentDirectory + "UserList.xml";
        public static Dictionary<int, string> UserList { get; set; }
        /// <summary>
        /// Возвращает строки с текущей погодой по заданному месту до заданного времени
        /// </summary>
        /// <param name="LocationName">Именное обозначение пункта для поиска</param>
        /// <param name="parameter">До кокого времени погоду</param>
        /// <returns></returns>
        public static string GetWeatherString(string LocationName, DaysAfter parameter = DaysAfter.Today)
        {
            string returnString = $"*{LocationName}:*" + Environment.NewLine;
            foreach (var Item in GetWeatherData(LocationName))
            {
                if (Item.Key.Day <= DateTime.Today.AddDays((int)parameter).Day)
                {
                    returnString += Item.Value + Environment.NewLine;
                }
            }
            return returnString;
        }
        /// <summary>
        /// Возвращает cловарь с текущей погодой по заданному месту
        /// </summary>
        /// <param name="LocationName">Именное обозначение пункта для поиска</param>
        /// <returns></returns>
        private static Dictionary<DateTime, string> GetWeatherData(string LocationName)
        {
            MapPoint LocationPoint = GetLocationPoint(LocationName);
            string url = $"http://api.openweathermap.org/data/2.5/forecast?lat={LocationPoint.Latitude}&lon={LocationPoint.Longitude}&appid={Settings.WeatherMapApiKey}&mode=xml";

            Dictionary<DateTime, string> WeatherData = new Dictionary<DateTime, string>();

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            using (WebResponse request = webRequest.GetResponse())
            {
                using (Stream stream = request.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        XmlDocument doc = new XmlDocument();
                        string data = reader.ReadToEnd();
                        doc.LoadXml(data);
                        string townName = doc.SelectSingleNode("//location/name").InnerText;
                        var nodes = doc.SelectNodes("//time"); //   //*[@id="collapsible4"]/div[2]/div/span[2]/span[1]/span[2]
                        foreach (var Item in nodes)
                        {
                            XmlDocument tempXML = new XmlDocument();
                            tempXML.LoadXml((Item as XmlNode).OuterXml);

                            DateTime time = DateTime.Parse(tempXML.SelectSingleNode("//time").Attributes["from"].Value);
                            string  windDirection, windSpeed, temperature, humidity, clouds, symbol;

                            symbol = tempXML.SelectSingleNode("//symbol").Attributes["name"].Value;
                            windDirection = tempXML.SelectSingleNode("//windDirection").Attributes["code"].Value;
                            windSpeed = tempXML.SelectSingleNode("//windSpeed").Attributes["mps"].Value;
                            temperature = tempXML.SelectSingleNode("//temperature").Attributes["value"].Value;
                            humidity = tempXML.SelectSingleNode("//humidity").Attributes["value"].Value;
                            clouds = tempXML.SelectSingleNode("//clouds").Attributes["value"].Value;
                            string returnString = $"{time.ToShortDateString()} {time.TimeOfDay.Hours}:00-{time.AddHours(3).TimeOfDay.Hours}:00 "
                                + $"{symbol}, t={temperature.ToCelsius()}c, ветер {windDirection.Translate()}{windSpeed}м/с, {clouds.Translate()}, влажность {humidity}%";
                            WeatherData.Add(time, returnString);
                        }
                    }
                }
            }
            return WeatherData;
        }
        /// <summary>
        /// Вовращает долгоду и широту классом MapPoint
        /// </summary>
        /// <param name="LocationName">Именное обозначение пункта для поиска</param>
        /// <returns></returns>
        private static MapPoint GetLocationPoint(string LocationName)
        {
            string ApiKey = Settings.GoogleApiKey;
            GoogleLocationService locationService = new GoogleLocationService();
            return locationService.GetLatLongFromAddress(LocationName);
        }
        [Flags]
        public enum DaysAfter
        {
            Today = 0,
            Tomorrow = 1,
            DayAfterTomorrow = 2
        }
        /// <summary>
        /// Сохраняет данные о пользовавшихся пользователях
        /// </summary>
        public static void SaveData()
        {
            using (Stream stream = File.Open(Path, FileMode.Create))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, UserList);
            }
        }
        /// <summary>
        /// Загружает данные о уже пользовавшихся пользователях
        /// </summary>
        public static void LoadData()
        {
            if (!File.Exists(Path))
            {
                UserList = new Dictionary<int, string>();
            }
            else
                using (Stream stream = File.Open(Path, FileMode.Open))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    UserList = (Dictionary<int, string>)formatter.Deserialize(stream);
                }
        }
    }
}