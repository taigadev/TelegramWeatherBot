﻿using System;

namespace TelegramWeatherBot.Core
{
    /// <summary>
    /// Класс с дополнительными расширениями
    /// </summary>
    public static class Extentions
    {
        /// <summary>
        /// переводит строку с числовым содержимым из градусов Кельвина в Цельсий
        /// </summary>
        /// <param name="self"></param>
        /// <returns>Градусы цельсия с типом string</returns>
        internal static string ToCelsius(this string self)
        {
            double value = Convert.ToDouble(self.Replace('.', ','));
            value = value - 273.15; // celsii (градусы по Фаренгейту - 273.15)
            return (Math.Round(value, 1)).ToString();
        }
        internal static string Translate(this string self)
        {
            string RUtext;
            switch (self)
            {
                // погода
                case "N": RUtext = "северный "; break;
                case "NE": RUtext = "северо-западный "; break;
                case "NNE": RUtext = "северо-западный "; break;
                case "E": RUtext = "западный "; break;
                case "SE": RUtext = "южно-западный "; break;
                case "SSE": RUtext = "южно-западный "; break;
                case "S": RUtext = "южный "; break;
                case "SW": RUtext = "южно-восточный "; break;
                case "SSW": RUtext = "южно-восточный "; break;
                case "W": RUtext = "восточный "; break;
                case "NW": RUtext = "северо-восточный "; break;
                case "NNW": RUtext = "северо-восточный "; break;

                case "clear sky": RUtext = "чистое небо";break;
                case "few clouds": RUtext = "облачно";break;
                case "broken clouds": RUtext = "местами облачно"; break;
                case "scattered clouds": RUtext = "густые облака";break;
                case "overcast clouds": RUtext = "пасмурно";break;
                case "moderate rain": RUtext = "умеренный дождь";break;
                case "light rain": RUtext = "легкий дождь";break;
                    
                default: RUtext = ""; break;
            }

            return RUtext;
        }
    }
}