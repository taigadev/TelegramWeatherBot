﻿using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramWeatherBot.Types;

namespace TelegramWeatherBot.Core.Commands
{
    public class CmHello : Command
    {
        public override string Name => "hello";
        public override string Response => "Light curse you!";

        public override async void Execute(Message message, TelegramBotClient Client)
        {
            var chatId = message.Chat.Id;
            var messageId = message.MessageId;
            await Client.SendTextMessageAsync(chatId, Response, replyToMessageId: 0);
        }
    }
}
