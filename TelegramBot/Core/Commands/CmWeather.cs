﻿using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramWeatherBot.Types;

namespace TelegramWeatherBot.Core.Commands
{
    public class CmWeather : Command
    {
        public override string Name => "weather";
        public override string Response => null;
        public override async void Execute(Message message, TelegramBotClient Client)
        {
            var chatId = message.Chat.Id;
            var messageId = message.MessageId;
            var userId = message.From.Id;
            string userLocation = null;

            if (message.Text.Contains(" "))
            {
                string[] mass = message.Text.Split(' ');
                userLocation = mass[1];
                
                if (Weather.UserList.ContainsKey(userId))
                {
                    Weather.UserList[userId] = userLocation;
                }
                else
                {
                    Weather.UserList.Add(userId, userLocation);
                }
            }
            else
            {
                if (Weather.UserList.ContainsKey(userId))
                {
                    userLocation = Weather.UserList[userId];
                }
                else
                {
                    await Client.SendTextMessageAsync(chatId, "напишите город после /weather");
                    return;
                }
            }
            await Client.SendTextMessageAsync(chatId, Weather.GetWeatherString(userLocation), Telegram.Bot.Types.Enums.ParseMode.Markdown);

        }
    }
}
