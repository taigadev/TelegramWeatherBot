﻿using System;
using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramWeatherBot.Types;

namespace TelegramWeatherBot.Core.Commands
{
    public class CmHelp : Command
    {
        public override string Name => "help";

        public override string Response => "*/weather _town_* - погода в заданном населенном пункте" +
            Environment.NewLine + "*/hello* - привет";
        
        public override async void Execute(Message message, TelegramBotClient Client)
        {
            var chatId = message.Chat.Id;
            var messageId = message.MessageId;
            await Client.SendTextMessageAsync(chatId, Response, Telegram.Bot.Types.Enums.ParseMode.Markdown);
        }
    }
}
